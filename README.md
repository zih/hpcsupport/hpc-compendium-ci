# hpc-compendium-ci

This repository provides the `.gitbal-ci.yaml` file for the *hpc-compendium* project.  By managing
the `.gitlab-ci.yaml` file within in this project with quite strict accesses rules, we can protect
it from change. Refer to
https://docs.gitlab.com/ee/ci/environments/deployment_safety.html#protect-gitlab-ciyml-from-change
for further information.
